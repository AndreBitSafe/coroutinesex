package com.studying.coroutines.flow.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlin.random.Random

object DemoData {

    fun generate(): Flow<Person> {
        val delay = Random(100).nextLong(2000)
        Thread.sleep(delay)
        return listOf<Person>(
            Person(name = "Аврора", phone = "+380964668352"),
            Person(name = "Агата", phone = "+380957768352"),
            Person(name = "Агафья", phone = "+380634668990"),
            Person(name = "Аглая", phone = "+380504634352"),
            Person(name = "Агния", phone = "+380964668234"),
            Person(name = "Ада", phone = "+380734668882"),
            Person(name = "Аделаида", phone = "+380504668912"),
            Person(name = "Адель", phone = "+380964668347"),
            Person(name = "Адина", phone = "+380969668342"),
        ).asFlow()
    }
}