package com.studying.coroutines.flow.data

data class Person(
    val name: String,
    val phone: String
) {

    override fun toString() = """|$name
        |$phone""".trimMargin()
}
