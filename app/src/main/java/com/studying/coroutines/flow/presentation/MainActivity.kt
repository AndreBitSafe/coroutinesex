package com.studying.coroutines.flow.presentation

import android.os.Bundle
import android.text.Editable
import androidx.appcompat.app.AppCompatActivity
import com.studying.coroutines.flow.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    private val presenter = MainPresenter()

    private val editTextListener = object : MyTextWatcher() {
        override fun afterTextChanged(p0: Editable?) {
            val userInput = user_input.text.toString()
            if (previousData != userInput) {
                previousData = userInput
                presenter.search(query = userInput)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        user_input.addTextChangedListener(editTextListener)
    }

    override fun onStart() {
        super.onStart()
        presenter.attach(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.detach()
    }

    override fun show(result: String) {
        search_result.text = result
    }

    override fun showSearchError() {
        search_result.text = "Ничего не найденно"
    }
}