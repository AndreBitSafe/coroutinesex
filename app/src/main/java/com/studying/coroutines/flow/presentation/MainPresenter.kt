package com.studying.coroutines.flow.presentation

import com.studying.coroutines.flow.data.DemoData
import com.studying.coroutines.flow.data.Person
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class MainPresenter : CoroutineScope {

    private var view: MainView? = null

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    private val mySharedFlow = MutableSharedFlow<String>()

    private val mainJob = launch(start = CoroutineStart.LAZY) {
        mySharedFlow.debounce(1000)
            .map { query ->
                getFilteredData(query)
            }
            .collect { result ->
                if (result.isNotEmpty())
                    view?.show(result)
                else
                    view?.showSearchError()
            }
    }

    fun search(query: String) {
        launch { mySharedFlow.emit(query) }
    }

    fun attach(view: MainView) {
        this.view = view
        mainJob.start()
    }

    private suspend fun getFilteredData(query: String): String {
        return DemoData.generate()
            .filter {
                it.isStartedWith(query = query)
            }
            .fold("") { acc, value ->
                "$acc \u27bb $value\n"
            }
    }

    private fun Person.isStartedWith(query: String): Boolean {
        val name = this.name.toLowerCase(Locale.ROOT)
        val phone = this.phone.toLowerCase(Locale.ROOT)
        val inputQuery = query.toLowerCase(Locale.ROOT)
        return name.startsWith(inputQuery) || phone.startsWith(inputQuery)
    }


    fun detach() {
        coroutineContext.cancelChildren()
        view = null
    }
}