package com.studying.coroutines.flow.presentation

interface MainView {

    fun show(result: String)
    fun showSearchError()
}