package com.studying.coroutines.flow.presentation

import android.text.TextWatcher

abstract class MyTextWatcher : TextWatcher {

    protected var previousData: String = ""

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}